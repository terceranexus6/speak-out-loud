# Speak out loud

## LICENSE

This work is licensed under [CC-BY-SA](https://raw.githubusercontent.com/imthenachoman/How-To-Secure-A-Linux-Server/master/LICENSE.txt) and any source code included in the PDFs is licensed under [GPLv3](https://choosealicense.com/licenses/gpl-3.0/).
